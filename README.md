##Framework

	This is a capybara cucumber based framework without rails

##Pre-requisite

	You need to have Ruby 1.9.3 or above and following gems installed

		  require 'capybara'
		  require 'capybara/cucumber'
		  require 'cucumber'
		  require 'site_prism'
		  require 'selenium-webdriver'
		  require 'rspec/expectations'
		  require 'data_magic'
		  require 'capybara-screenshot/cucumber'
	
	Chrome and chrome webdriver installed, with proper path 

##Clone the repository

    $ git clone https://ashish0889@bitbucket.org/ashish0889/amazontestsuite.git
    $ cd amazontestsuite

##You may widh to do bundle update before running cucumber

    $ bundle update

##Now run cucumber to see all the scenario passes using chrome.

    $ bundle exec cucumber