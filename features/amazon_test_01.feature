Feature: to validate today deal page and search product and add to cart

  Background: goto amazon home page
    Given goto amazon homepage

  Scenario: goto today deals page and filter for books
    When user clik on today deal button from homepage
    Then user should be redirected to today deal page
    And user select books checkbox from department filter
    Then page should be reloaded with books products
    And print first page all products

  Scenario: search for a book and add to cart
    When user search for a book from home page
    Then user should be redirected to result page
    And user click on first item from result
    Then user should be redirected to product page
    And user click on add to cart page
    Then product should be added to cart
    When user click on cart button
    Then user should be redirected to cart page with product in it