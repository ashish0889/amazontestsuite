Before do | scenario |
  Capybara.current_driver = :selenium
  @app_host = $staging_machine_url
  @app_host = "http://#{$url_using}"
  Capybara.app_host = @app_host
  @wait = Selenium::WebDriver::Wait.new timeout: 120
  @driver = page.driver
  @wait.until { page.driver.browser.manage.window }
end

After do | scenario |
  p "in after callback.."
  if scenario.failed?
    Capybara::Screenshot.screenshot_and_save_page
  elsif scenario.passed?
    p "scenario has been passed..!!"
  end
  page.driver.quit
end