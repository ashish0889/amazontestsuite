  require 'capybara'
  require 'capybara/cucumber'
  require 'cucumber'
  require 'site_prism'
  require 'selenium-webdriver'
  require 'rspec/expectations'
  require 'data_magic'
  require 'capybara-screenshot/cucumber'
  $Log = {}
  DataMagic.yml_directory = './features/config/data'

  path =  File.absolute_path('screenshots/failed/')
  p path
  p FileUtils.rm_rf(path, secure: true)

  Capybara.save_path = "./screenshots/failed"

  $url_using = 'amazon.com'

  Capybara.default_driver = :seleniumn
  Capybara.register_driver :selenium do |app|
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.read_timeout = 120
    caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => [ "--start-maximized" ]})
    options = { :browser => :chrome, :desired_capabilities => caps, http_client: client }

    Capybara::Selenium::Driver.new(app, options)
  end

