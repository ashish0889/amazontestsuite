Given("goto amazon homepage") do
  visit '/'
end

When("user clik on today deal button from homepage") do
	@homepage = HomePage.new
	@homepage.click_today_deals
end

Then("user should be redirected to today deal page") do
	@todaydeals = TodayDealsPage.new
	expect(@todaydeals.verify_today_page).to be true
end

Then("user select books checkbox from department filter") do
	@todaydeals.checkbox_books.click
end

Then("page should be reloaded with books products") do
	expect(@todaydeals.filter_result_summary.text == 'Books').to be true
end

Then("print first page all products") do
	@todaydeals.get_all_deals_price
	p $Log
end

When("user search for a book from home page") do
	@homepage = HomePage.new
	@item = "The Cucumber Book"
	@homepage.search_item(@item)
end

Then("user should be redirected to result page") do
	@search_list_page = SearchListPage.new
	expect(@search_list_page.has_search_div?).to be true
end

Then("user click on first item from result") do
	@item_clicked_text = @search_list_page.item_listed_titles[0].text
	@search_list_page.item_listed_titles[0].click
end

Then("user should be redirected to product page") do
	@product_page = ProductPage.new
	expect(@product_page.product_title.text == "#{@item_clicked_text}").to be true
	expect(@product_page.has_product_div?).to be true
end

Then("user click on add to cart page") do
	@product_page.add_to_cart.click
end

Then("product should be added to cart") do
	expect(@product_page.cart_count.text == '1').to be true
end

When("user click on cart button") do
	@product_page.cart_icon.click
end

Then("user should be redirected to cart page with product in it") do
	@cart_page = CartPage.new
	expect(@cart_page.header.text == 'Shopping Cart').to be true
	expect(@cart_page.product_title.text == "#{@item_clicked_text}").to be true
end
