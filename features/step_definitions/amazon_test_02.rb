Given("user is on sign up page") do
	@homepage = HomePage.new
	@homepage.account_list.hover
	@homepage.new_user_link.click
end

When(/^user sign up using (.*)$/) do |req|
	@requirements = Req.new.load(req)
	@homepage.sign_up_user(@requirements)
end

When(/^user should be on verify email page$/) do
  expect(@homepage.verify_confirmation_page?).to be true
end

Given("user hover on all drop down") do
	@homepage = HomePage.new
	#binding.pry
	@homepage.departments_dropdown.hover
end

Given("use redirect to computer department from drop down menu") do
	@homepage.computer_option.click
end

When("user select computer component from side bar filter") do
	@search_page = SearchListPage.new
	@search_page.computer_components.click
end

Then("user select four star and up filter") do
	@search_page.four_star_filter.click
end

Then("page should be reloaded with only four star computer component") do
	expect(@search_page.verify_rating).to be true
	p $Log
end