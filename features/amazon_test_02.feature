Feature: to sign up user and validate filter in computer departarment

  Background: goto amazon home page
    Given goto amazon homepage

  Scenario: sign up validation
   Given user is on sign up page
   And user sign up using requirement_1
   Then user should be on verify email page

  Scenario: goto Computers department and use filter to find 4 star and up products
    Given user hover on all drop down
    And use redirect to computer department from drop down menu
    And user select computer component from side bar filter
    And user select four star and up filter
    Then page should be reloaded with only four star computer component
