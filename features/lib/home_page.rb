class HomePage < SitePrism::Page

  set_url  "/"

  element :today_deals, :xpath, "//a[contains(text(),\"Today's Deals\")]"
  element :search_box, '#twotabsearchtextbox'
  element :search_button, '.nav-search-submit'
  element :account_list, '#nav-link-accountList'
  element :new_user_link, '#nav-flyout-ya-newCust a'
  element :new_user_form, '#ap_register_form'
  element :new_user_name, '#ap_customer_name'
  element :new_user_email, '#ap_email'
  element :new_user_pass, '#ap_password'
  element :new_user_pass_check, '#ap_password_check'
  element :new_user_submit, '#continue'
  element :confirmation_page, '#cvf-page-content'
  element :confirmation_page_verify_email, '#cvf-page-content h1'
  element :all_drop_down, '#searchDropdownBox'
  element :departments_dropdown, :xpath, '//span[contains(text(),"Departments")]'
  element :computer_option, :xpath, '//div[@id="nav-flyout-shopAll"]//span[contains(text(),"Computers")]'

  def click_today_deals
    wait_until_today_deals_visible 10
    today_deals.click
  end

  def search_item(item)
  	wait_until_search_box_visible 10
  	search_box.send_keys "#{item}"
  	search_button.click
  end

  def sign_up_user(req)
  	wait_until_new_user_form_visible 10
  	new_user_name.set 'dummy'
  	new_user_email.set req['email']
  	new_user_pass.set req['password']
  	new_user_pass_check.set req['password']
  	new_user_submit.click
  end

  def verify_confirmation_page?
  	wait_until_confirmation_page_visible 10
  	if has_confirmation_page_verify_email?
  		true
  	else
  		false
  	end
  end
end