class CartPage < SitePrism::Page

  element :product_title, '.sc-product-title'
  elements :item_listed_div, '#sc-active-cart'
  element :header, '#sc-active-cart h2'
  element :item_listed_title, '.s-result-list .s-result-item h2'


end