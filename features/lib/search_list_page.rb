class SearchListPage < SitePrism::Page

  element :search_div, '#search'
  elements :item_listed_div, '.s-result-list .s-result-item'
  element :item_listed_image, '.s-result-list .s-result-item .s-image'
  elements :item_listed_titles, '.s-result-list .s-result-item h2'
  element :item_title, '.s-result-list .s-result-item h2'
  element :computer_components, :xpath, '//span[contains(text(),"Computer Components")]'
  element :four_star_filter, '.a-star-medium-4'
  #element :listed_item_rating, :xpath, '//span[contains(text(),"5 stars")]'
  element :listed_item_rating, '.a-row.a-size-small [aria-label*="5 stars"]'

  def verify_rating
    item_listed_div.each do |div|
      within div do
        title = item_title.text
        rating_text = listed_item_rating['aria-label']
        regex_for_number = /(\d+[.]\d)/
        rating_got = (regex_for_number.match rating_text)[0]
        if rating_got.to_i >= 4
          $Log[title] = rating_got
        else
          raise
        end
      end
    end
    return true
  end

end


#evaluate_script("document.querySelector('#{listed_item_rating.path}').textContent")