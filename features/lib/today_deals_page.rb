class TodayDealsPage < SitePrism::Page

  set_url  "/international-sales-offers/b/?ie=UTF8&node=15529609011&ref_=nav_navm_intl_deal_btn"

  element :checkbox_books, :xpath, "//label//span[contains(text(),'Books')]"
  elements :filter_result_bar, '.filterResultBar'
  element :page_banner, '.pageBanner h1'
  element :filter_result_summary , '#FilterItemView_all_summary .summary'
  elements :deal_view , '#widgetContent [id^="100_dealView_"]'
  element :deal_title, '#dealTitle'
  element :deal_price, '.priceBlock'

  def verify_today_page
    if has_page_banner?
	    if page_banner.text == 'Deals and Promotions'
	    	true
	    else
	    	false
	    end
	  else
	  	false
	  end
  end

  def get_all_deals_price
  	deal_view.each do |deal_ele|
  		within deal_ele do
  			if has_deal_title?
  				title = deal_title.text
  			end
  			if has_deal_price?
  				price = deal_price.text
  			end
  			$Log[title] = price
  		end
  	end
  end
end