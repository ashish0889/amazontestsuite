class Req

  include DataMagic
  DataMagic.load 'req.yml'

  def load(requirement_type)
    data_for "req/#{ requirement_type }"
  end
end
