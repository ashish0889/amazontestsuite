class ProductPage < SitePrism::Page

  element :product_title, '#productTitle'
  element :product_div, '#dp'
  element :add_to_cart, '#add-to-cart-button'
  element :cart_icon, '#nav-cart'
  element :cart_count, '#nav-cart-count'


end